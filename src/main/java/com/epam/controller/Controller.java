package com.epam.controller;

import com.epam.model.Domain;
import com.epam.model.MyTreeMap;

public class Controller {
    private Domain domain;

    public MyTreeMap<Integer, String> getInfoFromModel() {
        domain = new Domain();
        return domain.getInfo();
    }
}
