package com.epam.model;

public class Domain {
    private MyTreeMap<Integer, String> treeMap;

    public MyTreeMap<Integer, String> getInfo() {
        treeMap = new MyTreeMap<>();
        treeMap.put(1, "a");
        treeMap.put(2, "b");
        treeMap.put(5, "c");
        treeMap.put(3, "c");
        treeMap.remove(5);
        return treeMap;
    }
}
