package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class MyTreeMap<K, V> implements Map<K, V> {
    private static Logger logger = LogManager.getLogger("InfoForUser");
    private static final boolean RED = true;
    private static final boolean BLACK = false;

    static class Node<K, V> {
        K key;
        V value;
        boolean color;
        Node<K, V> left;
        Node<K, V> right;

        public Node(K key, V value, boolean color) {
            this.key = key;
            this.value = value;
            this.color = color;
        }
    }

    private Node<K, V> root;
    private int size;


    public MyTreeMap() {
        root = null;
        size = 0;
    }

    /**
     * Compare two keys.
     *
     * @param key1
     * @param key2
     * @return result of comparison.
     */
    private int compare(K key1, K key2) {
        try {
            Comparable<? super K> cmpKey1 = (Comparable<? super K>) key1;
            return cmpKey1.compareTo(key2);
        } catch (ClassCastException e) {
            throw new ClassCastException(e.getMessage());
        }
    }

    /**
     * @param node to check.
     * @return true id color is red.
     */
    private boolean isRed(Node<K, V> node) {
        if (node == null) {
            return false;
        }
        return node.color;
    }

    /**
     * Left-rotation.
     */
    private Node<K, V> rotateLeft(Node<K, V> node) {
        if (node != null && isRed(node.right)) {
            Node<K, V> temp = node.right;
            node.right = temp.left;
            temp.left = node;

            temp.color = temp.left.color;
            temp.left.color = RED;
            return temp;
        } else {
            return null;
        }
    }

    /**
     * Right-rotation.
     */
    private Node<K, V> rotateRight(Node<K, V> node) {
        if (node != null && isRed(node.left)) {
            Node<K, V> temp = node.left;
            node.left = temp.right;
            temp.right = node;

            temp.color = temp.right.color;
            temp.right.color = RED;
            return temp;
        }
        {
            return null;
        }
    }

    /**
     * Color flip.
     */
    private void flipColor(Node<K, V> node) {
        if ((node != null && node.right != null && node.left != null)
                && ((isRed(node.right) && isRed(node.left) && !isRed(node)) || isRed(node))) {
            node.color = !node.color;
            node.left.color = !node.left.color;
            node.right.color = !node.right.color;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object o) {
        if ((K) o == null) {
            throw new NullPointerException("Key can`t be NULL");
        }
        Node<K, V> current = root;
        while (current!= null) {
            int compareResult = compare((K)o, current.key);
            if(compareResult < 0) {
                current = current.left;
            } else if(compareResult > 0) {
                current = current.right;
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object o) {
        return false;
    }

    @Override
    public V get(Object o) {
        if ((K) o == null) {
            throw new NullPointerException("Key can`t be NULL");
        }
        Node<K, V> current = root;
        while (current!= null) {
            int compareResult = compare((K)o, current.key);
            if(compareResult < 0) {
                current = current.left;
            } else if(compareResult > 0) {
                current = current.right;
            } else {
                return current.value;
            }
        }
        return null;
    }

    @Override
    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("Key can`t be NULL");
        }
        root = put(root, k, v);
        root.color = BLACK;
        size++;
        return v;
    }

    /**
     * Utility function which helps put new nodes.
     */
    private Node<K, V> put(Node<K, V> node, K key, V value) {
        if (node == null) {
            return new Node<>(key, value, RED);
        }

        int comparisonResult = compare(key, node.key);

        if (comparisonResult < 0) {
            node.left = put(node.left, key, value);
        } else if (comparisonResult > 0) {
            node.right = put(node.right, key, value);
        } else {
            node.value = value;
        }

        //if tree isn`t black-red, fix it
        if (isRed(node.right) && isRed(node.left)) {
            node = rotateLeft(node);
        }
        if (isRed(node.left) && isRed(node.left.left)) {
            node = rotateRight(node);
        }
        if (isRed(node.left) && isRed(node.right)) {
            flipColor(node);
        }
        return node;
    }

    @Override
    public V remove(Object o) {
        if (((K) o == null)) {
            throw new NullPointerException("Key can`t be NULL");
        }
        if(containsKey((K)o)) {
            V val = get(o);

            if(!isRed(root.right) && !isRed(root.left)) {
                root.color = RED;
            }

            root = remove(root, (K)o);
            size--;

            if(isEmpty()) {
                root.color = BLACK;
            }
            return val;
        }
        return null;
    }

    private Node<K, V> remove(Node<K, V> node, K key) {
        if (compare(key, node.key) < 0) {
            if (!isRed(node.left) && !isRed(node.left.left))
                node = moveRedLeft(node);
            node.left = remove(node.left, key);
        } else {
            if (isRed(node.left))
                node = rotateRight(node);
            if (compare(key, node.key) == 0 && (node.right == null))
                return null;
            if (!isRed(node.right) && !isRed(node.right.left))
                node = moveRedRight(node);
            if (compare(key, node.key) == 0) {
                Node<K, V> x = min(node.right);
                node.key = x.key;
                node.value = x.value;
                // node.val = get(node.right, min(node.right).key);
                // node.key = min(node.right).key;
                node.right = removeMin(node.right);
            } else
                node.right = remove(node.right, key);
        }
        return balance(node);
    }

    private Node<K, V> min(Node<K, V> node) {
        if (node.left == null)
            return node;
        else
            return min(node.left);
    }

    private Node<K, V> removeMin(Node<K, V> node) {
        if (node.left == null)
            return null;

        if (!isRed(node.left) && !isRed(node.left.left))
            node = moveRedLeft(node);

        node.left = removeMin(node.left);
        return balance(node);
    }


    private Node<K, V> moveRedRight(Node<K, V> node) {
        assert (node != null);
        assert isRed(node) && !isRed(node.right) && !isRed(node.right.left);
        flipColor(node);
        if (isRed(node.left.left)) {
            node = rotateRight(node);
            flipColor(node);
        }
        return node;
    }

    private Node<K, V> moveRedLeft(Node<K, V> node) {
        assert (node != null);
        assert isRed(node) && !isRed(node.left) && !isRed(node.left.left);

        flipColor(node);
        if (isRed(node.right.left)) {
            node.right = rotateRight(node.right);
            node = rotateLeft(node);
            flipColor(node);
        }
        return node;
    }


    /**
     * restore red-black tree invariant
     */
    private Node<K, V> balance(Node<K, V> node) {
        assert (node != null);
        if (isRed(node.right))
            node = rotateLeft(node);
        if (isRed(node.left) && isRed(node.left.left))
            node = rotateRight(node);
        if (isRed(node.left) && isRed(node.right))
            flipColor(node);

        return node;
    }

    public void print() {
        inorder(root);
    }

    private void inorder(Node<K,V> r) {
        if(r != null) {
            inorder(r.left);
            logger.info("[" + r.key + " " + r.value + "]");
            inorder(r.right);
        }
    }
    @Override
    public void putAll(Map<? extends K, ? extends V> map) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }
}
