package com.epam.view;

public enum Menu {
    FIRST("1", "1-Print My own tree map"),
    SECOND("2", "2-Say Hello"),
    QUIT("Q", "Q-Quite");

    private final String key;
    private final String value;

    private Menu(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
