package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MyViewWithEnum {
    private static Logger logger = LogManager.getLogger("InfoForUser");
    private Scanner scanner = new Scanner(System.in);
    private Controller controller;

    private void printMenu() {
        logger.info("\nMENU\n");
        logger.info(Menu.FIRST.getValue() + "\n");
        logger.info(Menu.SECOND.getValue() + "\n");
        logger.info(Menu.QUIT.getValue() + "\n");
    }

    private void hello() {
        logger.info("Enter you name");
        String name = scanner.next();
        logger.info("Hello " + name);
    }

    public final void show() {
        String key;
        do {
            printMenu();
            logger.info("Make choice");
            key = scanner.next();

            if(key.equalsIgnoreCase(Menu.FIRST.getKey())) {
                controller.getInfoFromModel().print();
            } else if(key.equalsIgnoreCase(Menu.SECOND.getKey())){
                hello();
            }

        } while (!key.equalsIgnoreCase("Q"));
    }

}
