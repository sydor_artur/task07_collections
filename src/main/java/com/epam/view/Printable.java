package com.epam.view;

/**
 * This interface help to print information,
 * according to user choice.
 */
@FunctionalInterface
public interface Printable {
    /**
     * Print information from model.
     */
    void print();
}

