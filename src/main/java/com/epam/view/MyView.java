package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger = LogManager.getLogger("InfoForUser");
    /**
     * controller link model and view.
     */
    private Controller controller;
    /**
     * This variable saves menu  information.
     */
    private Map<String, String> menu;
    /**
     * This variable link menu and user choice.
     */
    private Map<String, Printable> methodMenu;
    /**
     * Take user input.
     */
    private Scanner scanner = new Scanner(System.in);

    /**
     * Initialise menu.
     */
    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menu.put("1", "1-Print My own tree map");
        menu.put("2", "2-Say Hello");
        menu.put("Q", "Q-Quite");

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::pressButton1);
        methodMenu.put("2", this::pressButton2);
    }

    /**
     * This method show all stones in the set.
     */
    private void pressButton1() {
        controller.getInfoFromModel().print();
    }

    /**
     * Show general weight of set.
     */
    private void pressButton2() {
        logger.info("Enter you name");
        String name = scanner.next();
        logger.info("Hello " + name);
    }

    /**
     * Print menu in console.
     */
    private void printMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    /**
     * This method control menu.
     */
    public final void show() {
        String key;
        do {
            printMenu();
            logger.info("Make choice");
            key = scanner.next().toUpperCase();
            try {
                methodMenu.get(key).print();
            } catch (Exception e) {
                logger.info("Invalid input...");
            }
        } while (!key.equals("Q"));
    }

}
